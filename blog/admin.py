from django.contrib import admin
from .models import Article, Category, IPAddress
from django.contrib import messages
from django.utils.translation import ngettext


# Register your models here.
def make_published(self, request, queryset):
    updated = queryset.update(status='p')
    self.message_user(request, ngettext(
        '%d مقاله منتشر شد.',
        '%d مقاله منتشر شدند.',
        updated,
    ) % updated, messages.SUCCESS)


make_published.short_description = 'انتشار مقالات منتشر شده'


def make_draft(self, request, queryset):
    updated = queryset.update(status='d')
    self.message_user(request, ngettext(
        '%d مقاله پیش نویس شد.',
        '%d مقاله پیش نویس شدند.',
        updated,
    ) % updated, messages.SUCCESS)


make_draft.short_description = 'پیش نویس مقالات منتشر شده'


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('position', 'title', 'slug', 'parent', 'status')
    list_filter = (['status'])
    search_fields = ('title', 'slug')
    prepopulated_fields = {'slug': ('title',)}


admin.site.register(Category, CategoryAdmin)


class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'thumbnail_tag', 'slug', 'author', 'publish','is_special', 'status', 'category_to_str')
    list_filter = ('publish', 'status', 'author')
    search_fields = ('title', 'description')
    prepopulated_fields = {'slug': ('title',)}
    ordering = ['-status', '-publish']
    actions = [make_published, make_draft]

    # transforming to blog/models in article model for displaying categories in home page
    # def category_to_str(self, obj):
    #     return ', '.join([category.title for category in obj.category_published()])
    # category_to_str.short_description = 'دسته بندی'


admin.site.register(Article, ArticleAdmin)
admin.site.register(IPAddress)
